#!/usr/bin/perl

# $Id: globals.t,v 1.1 2008/06/04 23:03:44 axs Exp $           
           
use strict;
use FindBin qw($Bin);
use lib "$Bin/../";
use Test::More tests => 3;;

BEGIN { use_ok('PRC::Globals')  }

my $g = PRC::Globals->new();
isa_ok($g,'PRC::Globals');

my $h=`hostname`; chomp $h;
is($g->hostname(),$h,'hostname check');
