#!/usr/bin/perl

# $Id: harness.t,v 1.1 2008/06/04 23:03:44 axs Exp $

use strict;
use Test::Harness;
use FindBin qw($Bin);
use lib "$Bin/../";


runtests( "$Bin/globals.t"
		  ,"$Bin/lock.t"
		  ,"$Bin/logger.t"
		  ,"$Bin/config.t"
		);
