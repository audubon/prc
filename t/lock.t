#!/usr/bin/perl

#
# $Id: lock.t,v 1.1 2008/06/04 23:03:44 axs Exp $
#

use strict;
use FindBin qw($Bin);
use lib "$Bin/../";


use Test::More tests => 12;

my $ttl  = 10;
my $wait = 20;

#test the module is there
BEGIN { use_ok('PRC::Lock');  }

# etst construction
my $lock = PRC::Lock->new({debug=>0
                                                   ,ttl => $ttl
                                                   ,waittime=> $wait
                                                   ,lockfile => '/tmp/testlockfile'
                                                  });
isa_ok($lock,'PRC::Lock');


#make sure it can acquire and relesae
can_ok($lock, qw/acquire release breaklock trylock removeoldlock/);


SKIP: {
        my %h = $lock->__lockowner();
        skip "removeoldlock() test because old lockfile not there",1
          unless( defined -l $lock->{lockfile} );

        skip "removeoldlock() test because old lockfile its being used",1
          if( `ps -p $h{pid}` && $? == 0 );


        $lock->removeoldlock();
        is(-l $lock->{lockfile},undef ,'lockfile removed with removeoldlock');
}



#test creating
$lock->acquire();
is(-l $lock->{lockfile},1,'lockfile created');

#test reading the symlink
my %h = $lock->__lockowner();

is_deeply( [ sort keys %h ], [ qw/bin epoch euid pid ttl/ ], 'lock owner hash keys');
is($h{ttl},$ttl ,'ttl key value check');
is($h{pid},$$ ,'pid key value check');
is($h{bin},$0 ,'bin key value check');


#test releasing
$lock->release();
is(-l $lock->{lockfile},undef ,'lockfile released');

#test creating
$lock->trylock();
is(-l $lock->{lockfile},1,'lockfile created with trylock');


diag("destroy the lock object");
my $tmp = $lock->{lockfile};
undef $lock;
#test recreat
is(-l $tmp,undef ,'object destroyed lockfile not there');
