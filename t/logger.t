#!/usr/bin/perl

#
#
#   Tue Jan  8 16:58:18 CST 2008
#
#   $Id: logger.t,v 1.1 2008/06/04 23:03:44 axs Exp $
#
#   Author:
#    $Author: axs $


use strict;
use FindBin qw($Bin);
use lib "$Bin/../";


use Test::More tests => 10;


#test the module is there
BEGIN { use_ok('PRC::Logger');  }

# etst construction
my $logger = PRC::Logger->new({ logdirname => '/tmp' });
isa_ok($logger,'PRC::Logger');


#make sure it can acquire and relesae
can_ok($logger, qw/start mkstdalias initiallog logargs showsettings/);

is($logger->{errfile},'logger.err','testing logger err file name');
is($logger->{logfile},'logger.log','testing logger log file name');
is($logger->{logdir},'/tmp','testing logdirname');


diag("removing logfiles that may be old");
eval{
	unlink "$logger->{logdir}/$logger->{logfile}";
	unlink "$logger->{logdir}/$logger->{errfile}";
};


diag("starting the logger");
$logger->start();


#test creating
is(-e '/tmp/logger.log',1,'logfile created');

print ATTERR 'test';
is(-e '/tmp/logger.err',1,'errfile created');



diag("alias stdout and stderr with log /err file handles");
$logger->mkstdalias();



my $testmsg =  'goes_to_log';

print $testmsg;
my $lastlog = `tail -1 /tmp/logger.log`; chomp $lastlog;
like  ($lastlog, qr/$testmsg/, 'test stdout to logfile');


warn $testmsg;
my $lasterr = `tail -1 /tmp/logger.err`; chomp $lasterr;
like  ($lasterr, qr/$testmsg/, 'test stderr to errfile');


END{
	eval{
		unlink "$logger->{logdir}/$logger->{logfile}";
		unlink "$logger->{logdir}/$logger->{errfile}";
	};
}
