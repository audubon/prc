#!/usr/bin/perl

#
#   $Id: config.t,v 1.1 2008/06/04 23:03:44 axs Exp $
#
#

use strict;
use lib '..';

use Test::More tests => 11;
$|++;

#test the module is there
BEGIN { use_ok('PRC::Config::XML');  }

# test construction
my $xmldata = q^
<config>
<dsn>IPPROT</dsn>
<dbuser>prov_dba</dbuser>
<dbpass>prov_pass</dbpass>
<dbtype>ADODB</dbtype>
<debug>1</debug>
</config>
^;

my $cf = PRC::Config::XML->new({xmlconf=>$xmldata});
isa_ok($cf,'PRC::Config::XML');


is($cf->get_dbuser,'prov_dba' ,'dbuser test');
is($cf->get_dbpass,'prov_pass' ,'dbpass test');
is($cf->get_dbtype,'ADODB' ,'dbtype test');
is($cf->get_dsn,'IPPROT' ,'dsn test');


is_deeply(  [ sort $cf->getConfKeys() ]
                   ,[ sort qw/dbpass dbtype dbuser debug dsn/ ]
                   ,'conf hash keys');


is($cf->set_dsn('NEWDSN'),'NEWDSN' ,'new dsn test');
is($cf->set_dbtype('NEWtype'),'NEWtype' ,'new type test');
is($cf->set_dbpass('NEWpass'),'NEWpass' ,'new pass test');
is($cf->set_dbuser('NEWuser'),'NEWuser' ,'new user test');



1;
