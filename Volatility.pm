#!/usr/bin/perl


package PRC::Volatility;

# sqrt256 * std(ln[Pt/P(t-1)])
# Historic volatility is the standard deviation of the "price returns"
# over a given number of sessions, multiplied by a factor to produce
# an annualized volatility level. A "price return" is the natural
# logarithm of the percentage price changes or ln[Pt/P(t-1)].

#annualized historical volatility = square root of ( 250 * variance of natural log differences in daily prices for the calendar month)


require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = qw(historical_vol hist_vol);
use lib '/home/axs/work';
use Statistics::Descriptive;
use PRC::Util qw/logret/;
use strict;

our $VERSION = substr(q$Revision: 1.3 $, 10);


sub historical_vol {
    my ($rows,$short)=@_;

    unless(defined $short){
        return hist_vol($rows);
    }

    my @d =logret($rows);

    #long
    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@d);
    my $annv=100*sqrt(250 * ($stat->standard_deviation()**2));

    #short
    my $xstat = Statistics::Descriptive::Full->new();
    $xstat->add_data(splice(@d,- $short));
    my $sannv= 100*sqrt(250 * ($xstat->standard_deviation()**2));

    return wantarray ? ($annv,$sannv) : [$annv,$sannv];
}


#return just the historical volatility you asked for
sub hist_vol {
    my ($rows)=@_;

    my @d =logret($rows);

    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@d);
    my $annv=100*sqrt(250 * ($stat->standard_deviation()**2));

    return $annv;
}

1;

