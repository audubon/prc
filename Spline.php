<?php
/**
 *  Cubic Spline Interpolation of data
 *  Date: Mon Aug  6 16:42:36 CDT 2007
 *  $Id: Spline.php,v 1.1 2008/01/05 00:32:34 axs Exp $
 *
 *  converted from  http://search.cpan.org/~jarw/Math-Spline-0.01/Spline.pm
 */


class Spline{
  function Spline($x,$y){
    $this->hold    = array();
    $this->hold[0] = $x;
    $this->hold[1] = $y;
    $y2 = Derivative2($this->hold[0],$this->hold[1]);

    $this->hold[2] = $y2;
  }

  function evaluate($v) {
    $idx = binsearch( $this->hold[0], $v );
    return spline($this->hold[0],$this->hold[1],$this->hold[2],$idx,$v);
  }
}

function spline ($x,$y,$y2,$i,$v){
  $klo = $i;
  $khi = $i+1;
  $h = $x[$khi] - $x[$klo];
  if( $h == 0 ){
    exit( "Zero interval in spline data.\n" );
  }
  $a = ($x[$khi] - $v) / $h;
  $b = ($v - $x[$klo]) / $h;

  return $a * $y[$klo] + $b*$y[$khi]
    +(($a*$a*$a-$a)*$y2[$klo]
      +($b*$b*$b-$b)*$y2[$khi]) * ($h*$h)/6.0;
}

function binsearch($x,$v) {
  $klo=0;
  $khi = count($x);
  $k=null;
  while( ($khi-$klo) > 1 ){
    $k = ($khi+$klo) / 2;

   if( $x[$k] > $v ){
     $khi=$k;
   }
   else {
     $klo=$k;
   }
 }
 return $klo;
}
/*
 * second derivative of polynomial
 */
function Derivative2($x,$y,$yp1=null,$ypn=null) {
   $n  = count($x);
   $i  = null;
   $y2 = array();
   $u  = array();
   if( $yp1 == null ){
     $y2[0] = 0;
     $u[0]  = 0;
   }
   else {
     $y2[0] = -0.5;
     $u[0] = (3/($x[1] - $x[0]) ) * ( ($y[1] - $y[0] ) / ( $x[1] - $x[0] ) - $yp1);
   }

   for($i=1; $i<$n; $i++) {
     $sig = ($x[$i] - $x[$i-1]) / ($x[$i+1] - $x[$i-1]);

     $p = $sig * $y2[$i-1]+2.0;
     $y2[$i] = ($sig-1.0)/$p;
     $u[$i]  = (6.0*( ($y[$i+1]- $y[$i])/($x[$i+1]-$x[$i])-
                     ($y[$i] - $y[$i-1])/($x[$i]-$x[$i-1])
                     )/
               ($x[$i+1] - $x[$i-1])-$sig*$u[$i-1])/$p;
   }

   $qn =null;
   $un =null;
   if( $ypn == null ){
     $qn=0;
     $un=0;
   }
   else {
     $qn=0.5;
     $un=(3.0/($x[$n] - $x[$n-1]))*
       ($ypn-($y[$n] - $y[$n-1])/($x[$n] - $x[$n-1]));
   }

   $y2[$n]=($un-$qn * $u[$n-1]) / ($qn * $y2[$n-1]+1.0);

   for($i=$n-1; $i>=0; --$i) {
     $y2[$i]=$y2[$i]*$y2[$i+1]+$u[$i];
   }

   return $y2;
}

//print_r( Derivative2(array(1,2,53,3,4,4) , array(1,24,83,23,14,4) ) );

$x = array(1,2,3,4 ,5 ,6 ,7,8,9,10);
$y = array(4,6,7,14,15,12,8,6,9,15);
$sp = new Spline($x,$y);

for($p=1; $p<=count($x); $p+=.2 ){
  print $sp->evaluate($p);
  print "\n";
}


?>
