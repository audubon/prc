#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:
#   DATE:         6/11/06 4:55:09 PM
#   $Id: Util.pm,v 1.3 2007/05/07 23:13:21 axs Exp $
#
#---------------------------------------------------

package PRC::Util;


require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = qw(simpleret logret sma);
use strict;

our $VERSION = substr(q$Revision: 1.3 $, 10);


sub simpleret {
    my $r=shift;

    my @ret = ();
    if( ref $r->[0] ){
        @ret = map{ ($r->[$_][0]-$r->[$_-1][0])/$r->[$_-1][0] }1..$#{@$r};
    }
    else {
        @ret = map{ ($r->[$_]-$r->[$_-1])/$r->[$_-1] }1..$#{@$r};
    }


    return wantarray ? @ret : \@ret;
}


#log returns
sub logret {
    my $rows=shift;

    my @ret = ();
    #log returns
    if (ref $rows->[0]) {
        @ret =map{log($rows->[$_][0] / $rows->[$_-1][0]) }(1..$#{@$rows});
    }
    else {
       @ret = map{ log($rows->[$_] / $rows->[$_-1]) }(1..$#{@$rows});
    }

    return wantarray ? @ret : \@ret;
}


sub sma(){
    my $var=shift;
    my @d=@$var;

    my @ma=();

    for( my $i=0,my $k=3;$k<=$#d;$i++,$k++ ){
                my @group= @d[$i..$k];
                my $m = scalar @ma ?
                  $ma[$#ma]-($d[$i-1]/4)+($d[$k]/4) : sum(@group)/scalar @group;

                push @ma,$m;
    }

        return \@ma;
}


1;

__END__
gross ret= t / t-1
simple ret= (t - t-1)/t-1
log ret = log(t/t-1)

where log denotes a natural logarithm.

Suppose an asset has accumulated value of USD 100 at time t�1 and accumulated value USD 110 at time t. Then it had a

gross return of 1.100,

simple return of 0.100, and

log return of 0.095
