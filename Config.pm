#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:
#   DATE:         10/29/05 1:11:31 PM
#   $Id: Config.pm,v 1.2 2006/08/26 00:48:14 axs Exp $
#
#---------------------------------------------------

package PRC::Config;


require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = qw(readconfig);
use XML::Simple;
use Carp;
use FindBin qw($Bin $Script);
use strict;

our $VERSION = substr(q$Revision: 1.2 $, 10);


sub readconfig {
    my $c = shift || $Script;
    $c=~s/(.+)\..+$/$1\.conf/g;

    my $conf;

    if( -e $c ){
        $conf = $c
    }
    else {
        $conf="$Bin/$c";
    }

    return XMLin($conf);
}

1;
