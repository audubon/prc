#!/usr/bin/perl

#   DESCRIPTION:
#   DATE:         Mon Nov  6 15:23:06 CST 2006
#
#
#
#   $Id: Logger.pm,v 1.3 2008/06/04 23:01:38 axs Exp $
#
#   Author:
#    $Author: axs $
#



package PRC::Logger;



use strict;
use Carp;
use Fatal qw/open close/;
use PRC::Globals;


our $VERSION = substr(q$Revision: 1.3 $, 10);
our $AUTOLOAD;



sub new {
    my $proto = shift;
    my $args  = shift;
    my $class = ref($proto) || $proto;

        $args->{globals} ||= PRC::Globals->new( {config => $args->{config}} );

        #by default we log using  PRC::Logger::File
        my $logoutput = delete $args->{logdriver} || 'File';
        my $driver    = "PRC::Logger::$logoutput";


        #bring in the log driver you want to use
        eval "use $driver";
        if ($@) {
                Carp::croak $@;
        }


    my $self  = {
                                 #pass all args to the driver
                                 logdriver => $driver->new($args)
                                };

    bless $self,$class;

    return $self;
}


#----
# all calls to $self are passed to the driver object to handle
#----
sub AUTOLOAD{
        my $self = shift;
        my $type = ref($self) or croak "$self is not an object";

        my $name = $AUTOLOAD;
        $name =~ s/.*://;   # strip fully-qualified portion


        return undef if $name eq 'DESTROY';


        my $sub = $self->{logdriver}->can($name);

        if( ref $sub ){
                $self->{logdriver}->$sub(@_);
        }
        else {
                my @p = split(/=/,$self->{logdriver});
                Carp::carp "method $name is not implemented in $p[0] \n";
        }

}


1;


__END__

=head1 NAME

  PRC::Logger

=head1 SYNOPSIS

  use PRC::Logger
  my $logger= PRC::Logger->new({ logdriver => 'File'
                                                             ,logdirname => '/tmp'
                                });
  $logger->start();


  use PRC::Logger
  my $logger= PRC::Logger->new();



=head1 DESCRIPTION

This controls the logging funtionality implemented in the log driver classes
we use autoload to access the driver methods. By default we use the
PRC::Logger::File log driver

=head1 METHODS

=over

=item new( hashref )

all args are passed to the driver defined by the 'logdriver' key



=back

=head1 VERSION

$Id: Logger.pm,v 1.3 2008/06/04 23:01:38 axs Exp $

=cut
