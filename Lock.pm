#!/usr/bin/perl

#   DESCRIPTION:
#   DATE:         10/13/07 9:32:08 AM
#
#
#
#   $Id: Lock.pm,v 1.2 2008/06/04 23:01:38 axs Exp $
#
#


package PRC::Lock;

use strict;
#use Carp;

our $AUTOLOAD;
our $VERSION = substr(q$Revision: 1.2 $, 10,-1);


sub new{
    my $proto = shift;
    my $args  = shift;
    my $class = ref($proto) || $proto;

    my $self  = {
                                  waittime => $args->{waittime} || 20
                                 ,lockfile => $args->{lockfile} || '/tmp/lockfile'
                                 ,ttl      => $args->{ttl}          || 120
                                 ,debug    => $args->{debug}    || 0
                                 ,real_obj => delete $args->{obj}
        };

    bless $self,$class;

    return $self;
}


sub acquire{
        my $self  = shift;

        my $epoch    = time();
        my $sleeper  = 3;
        my $totsleep = 0;

        my %owner = $self->__lockowner();

        if( $owner{pid} == $$ ){
                $self->__printinfo("you own the lock already");
                #you own it already
                return 1;
        }

        #XXX
        while(1){
                if( 0 == symlink("pid=$$ ttl=$self->{ttl} euid=$> bin=$0 epoch=$epoch",$self->{lockfile}) ){
                        $totsleep += $sleeper;
                        $self->__printinfo("trying to create lock");
                        if( $totsleep > $self->{waittime} ){
                                $self->__printinfo("wait timed out");
                                return 0;
                        }
                        sleep $sleeper;
                }
                else {
                        return 1;
                }
        }
}


sub trylock{
        my $self = shift;
        my $epoch    = time();

        return symlink("pid=$$ ttl=$self->{ttl} euid=$> bin=$0 epoch=$epoch",$self->{lockfile});
}


sub release{
        my $self = shift;

        my %owner = $self->__lockowner();

        if(! exists $owner{pid} ){
                $self->__printinfo("lock does not exist ");
        }
        elsif( $owner{pid} == $$ ){
                $self->__printinfo("releasing lock");

                eval { unlink( $self->{lockfile} ); };
                if ($@){ $self->__printinfo($@) ;}
                else {
                        return 1;
                }
        }
        else {
                $self->__printinfo("cannot release lock you do not own");
        }
}


sub removeoldlock{
        my $self = shift;

        my %lockmeta = $self->__lockowner();
        unless( exists $lockmeta{pid} ){
                return undef;
        }

        if( ($lockmeta{ttl} + $lockmeta{epoch}) <  time() ){
                $self->__printinfo("ttl is too old; deleting lockfile \n");
                $self->breaklock();
        }
        elsif( `ps -p $lockmeta{pid}` && $? != 0 ){
                $self->__printinfo( "$lockmeta{pid} doesnt exist; deleting lockfile \n" );
                $self->breaklock();
        }

        return undef;
}


sub breaklock{
        my $self = shift;

        eval { unlink( $self->{lockfile} ); };
        if ($@){ $self->__printinfo($@) ;}
        else {
                return 1;
        }
}


sub __lockowner{
        my $self = shift;

        if( -l $self->{lockfile} ){
                my $f  = `ls -li $self->{lockfile}`;
                $f     =~ s/^\s+//;
                my @ef = split '->',$f;
                my @x  = split '\s', $ef[-1];
                my %t  = map{ /(.+)=(.+)/ }@x;
                return %t;
        }
        else {
                return 0;
        }
}


sub __printinfo(){
        my $self = shift;
        my $info = shift;

        my @d    = caller;
        print('line ',$d[2] ,': ', $info,"\n") if $self->{debug};
}


sub AUTOLOAD {
        my $self = shift;
        return unless defined ref $self->{realobj};
        my $method = $AUTOLOAD;
        $method =~ s/.*:://;
        warn("Proxying: $method\n") if $self->{debug};

        $self->acquire();
        $self->{real_obj}->$method(@_);
        $self->release();
}


sub DESTROY{
        my $self = shift;

        if( -l $self->{lockfile} ){
                $self->__printinfo('destroying object');
                eval { $self->release(); }
        }
}


1;


__END__


=head1 NAME

  PRC::Lock

=head1 SYNOPSIS

  use PRC::Lock;
  my $lock= PRC::Lock->new({debug=>1,ttl => 10, waittime=> 20 })

  $lock->acquire();
  ... do stuff ...
  $lock->release();

  my $proxylock = PRC::Lock->new({ obj => Blah->new() })

  #this call will be surrounded by acquire/release
  $proxylock->objmethd();


=head1 DESCRIPTION

This creates a lock using a symlink with attributes pid, ttl, epoch. In addition,
you can pass in an object that will be proxied between locking calls
All methods return 1 on success;

=head1 METHODS

=over

=item new()

This creates the object with default lockfile. hashref args optional

  waittime =>  how long to wait to obtain a lock.
  lockfile =>  the fullpath to the lockfile used
  ttl      => locks time to live
  debug    => boolean
  obj      => proxy an object methods between acquire and release calls

=item acquire()

creates the lock using symlink. result like this
lockfile -> pid=20102 ttl=120 euid=105 bin=o.pl epoch=1196286215

=item release()

deletes the lock if you own it

=item breaklock()

deletes the lock no matter what

=item trylock()

this trys to create a lock without waiting. it is somewhat like pthreads_mutex_trylock()

=item removeoldlock()

deletes the lock if the ttl os too old or the pid that created it is no longer running

=back

=head1 VERSION

$Id: Lock.pm,v 1.2 2008/06/04 23:01:38 axs Exp $

=cut
