#!/usr/bin/perl

#   DESCRIPTION:
#   DATE:         Thu Nov  1 16:12:51 CDT 2007
#   $Id: Password.pm,v 1.1 2008/06/04 23:01:38 axs Exp $
#
#
#   Requirements:
#         The authfile should be one space delimited record per line
#             server  username  passwd
#   Parameters:
#
#   Macros:
#
#   Formats:
#
#   Input:
#          server, username, [optional authfile]
#          if authfile is not passwd, will read authfile from etc/aurora.conf
#   Output:
#        passwd


package PRC::Password;


require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = ( qw/getpasswd/ );


use strict;
use Carp;
use Fatal       qw/open close/;
BEGIN {
        local $SIG{__DIE__};
    unless( eval "require PRC::ReadConf" ){
                Carp::carp "couldn't load PRC::ReadConf: $@";
    }
}

our $VERSION = substr(q$Revision: 1.1 $, 10,-1);


#----
#  passwwd = getpasswd(server,user,[ /full/path/to/passfile.txt ])
#  passfile defaults to etc/aurora.conf's authfile value
#----
sub getpasswd{
        my $server   = shift;
        my $user     = shift;
        my $passfile = shift || undef;

        my @record   = ();

        unless( defined $server ){
                Carp::croak "Server arg required $!";
        }
        unless( defined $user ){
                Carp::croak "User arg required $!";
        }

        # if no file passed try to get it from conf file
        unless( defined $passfile ){
                eval{
                        my %cfg = PRC::ReadConf::retenv();
                        $passfile = $cfg{'authfile'};
                };
        }

        unless (-e $passfile) {
                Carp::croak "Auth file $passfile does not exist $!";
        }


        open(F, $passfile );
        while( chomp(my $line=<F>) ){
                next unless $line =~/\S/;  #skip empty lines
                $line =~s/^\s+//;          #remove leading space
                next if $line =~/^#/;      #skip comments
                $line =~s/\s+$//;          #remove trailing space
                $line =~s/\s+/ /g;         #remove multispaces to one space

                @record = split(/\s/,$line);

                if( $record[0] eq $server && $record[1] eq $user ){
                        last;
                }
                else {
                        @record = ();
                }
    }
        close(F);
        return $record[2];
}


1;
