#!/usr/bin/perl

#
#   DESCRIPTION:
#   DATE:         Fri Nov 10 13:31:43 CST 2006
#   $Id: DirWatcher.pm,v 1.2 2008/06/04 23:01:38 axs Exp $
#
#
#   Author:
#    $Author: axs $
#


package PRC::DirWatcher;


use strict;
use Carp qw/croak carp/;
use Fatal       qw/open close/;
use POSIX 'setsid';



our $VERSION = substr(q$Revision: 1.2 $, 10,-1);


#---
# directory:   directory to watch
# runasdaemon: run program as a daemon (can optionally create custom daemon)
# processed:   hash to store processed items
# polltime:    sleep timer in loop
#---
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $args  = shift;

        croak "Do not call this directly, you must extend the class" if $class eq __PACKAGE__;
    croak "Directory required $!" unless defined $args->{directory};
        croak "$args->{directory} must be a directory" unless -d $args->{directory};

        my @dirstats = stat($args->{directory});


    opendir(my $dirhandle, $args->{directory}) or croak "cant opendir $args->{directory}: $!";


    my $self  = {
                                  directory   => $args->{directory} || undef
                                 ,dirhandle   => $dirhandle  || undef
                                 ,runasdaemon => $args->{runasdaemon} || 0
                                 ,processed   => $args->{processed} || {}
                                 ,polltime    => $args->{polltime} || 10
                                 ,debug       => $args->{debug} || 0
                                 ,dirmtime    => $dirstats[9] || 0
                                };

    bless $self,$class;

    return $self;
}


#---
# read the directory and for each file matching your filter() run prochandler()
#---
sub readDirectory {
    my $self = shift;

    my @f = grep { $self->filter($_) }readdir($self->{dirhandle});
    my @files =sort @f;

    rewinddir( $self->{dirhandle} );

        return \@files;
}

#---
# main loop
#---
sub start{
        my $self=shift;

        if( $self->{runasdaemon} ){
                $self->daemonize();
        }

        #intially process directory
        $self->processDir();

        while (1) {
                if( my $status = $self->stop() ){
                        print "stopping \n";
                        exit $status;
                }

                #check the modified time. if its new then reread directory
                if( $self->isModified() ){
                        $self->modifiedEvent();
                        print "rereading dir because mtime changed\n" if $self->{debug};
                }

                print "ran loop\n" if $self->{debug};
                sleep $self->{polltime};
        }
}


#----
# return boolean if dir modifed
#----
sub isModified{
        my $self =  shift;

        my @dirstats = stat($self->{directory});
        if( $dirstats[9] > $self->{dirmtime} ){
                $self->{dirmtime} = $dirstats[9];
                return 1;
        }
        else {
                return 0;
        }
}


#---
# make it a daemon
#---
sub daemonize {
        chdir '/'               or croak "Can't chdir to /: $!";
        open STDIN, '/dev/null' or croak "Can't read /dev/null: $!";
        open STDOUT, '>/dev/null' or croak "Can't write to /dev/null: $!";
        defined(my $pid = fork) or croak "Can't fork: $!";
        exit if $pid;
        setsid                  or croak "Can't start a new session: $!";
        open STDERR, '>&STDOUT' or croak "Can't dup stdout: $!";
}


#----
# this gets called once per loop if the directory is modified
#----
sub modifiedEvent{
        my $self = shift;
        $self->processDir();
}

#----
#
#----
sub processDir{
        my $self  = shift;
        my $files = $self->readDirectory();
        map{ $self->processFile($_) }@$files;
}


#---
# this stops the watcher if it returns true
#---
sub stop{
        return 0;
}

#---
# this filters the directory listing
#---
sub filter{
        my $self = shift;
        die "This is an abstract method. You must extend the class";
}

#---
# process each arg returned from filter()
#---
sub processFile{
        my $self = shift;
        die "This is an abstract method. You must extend the class";
}


1;


__END__


=head1 NAME

  PRC::DirWatcher

=head1 SYNOPSIS

 package MyWatcher;

 use PRC::DirWatcher v944;
 our @ISA = qw(PRC::DirWatcher);

 sub filter{
    my $self=shift;
    my $arg = shift;
    if(-f "$self->{directory}/$_" && not exists $self->{processed}{$_} ){
       return $arg;
    }
 }

 sub processFile {
    my $self=shift;
    my $arg=shift;
    print "$arg Called my proc handler \n";

    $self->{processed}{$arg}++;
 }

 #exit after 4 loops
 sub stop {
    my $self = shift;
    return 1 if $self->{count}++ > 4;
 }


 package main;

 my $r=MyWatcher->new({
          directory     =>'/home/sd/tmp'
          ,runasdaemon   => 0
          ,debug        => 1
          ,processed    => {
                        'ora.pl'=>1
                        ,'p.pl'=>1
                    ,'sqlnet.log'=>1
                       }
            }
        );
 $r->start();

=head1 DESCRIPTION

This is a base class that watches a directory for modified time changes and
calls user defined functions


=head1 METHODS

=over

=item new()

takes a hash reference

 directory =>   directory to watch
 runasdaemon => run program as a daemon (can optionally create custom daemon)
 processed =>   hash to store processed items
 polltime =>    sleep timer in loop
 debug => 1 or 0


=item stop()

this stops the watcher if it returns true
it would need to be implemented in a subclass.

=back


=head1 ABSTRACT METHODS

=over

=item filter()

each readDirectory() runs this to grep for the files you want

=item processFile()

this executes is executed on the files returned from filter()

=back


=head1 PRIVATE METHODS

=over

=item readDirectory()

read the directory and return arrayref of files matching your subclass->filter()


=item start()

this starts the watcher


=item isModified()

returns boolean. If the dirextory's mtime changed then this is true.

=item daemonize()

=item modifiedEvent()

this gets called once per loop if the directory is modified

=item processDir()

this calls readDirectory and runs processFile()

=back

=head1 VERSION

$Id: DirWatcher.pm,v 1.2 2008/06/04 23:01:38 axs Exp $

=cut
