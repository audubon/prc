#!/usr/bin/perl

#
#   DESCRIPTION:
#   DATE:         10/13/07 9:32:08 AM
#
#
#
#    $Id: File.pm,v 1.1 2008/06/04 23:03:17 axs Exp $
#    $Author: axs $
#



package PRC::Logger::File;


require v5.8.0;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw( *PRCLOG *PRCERR );  # symbols to export


use strict;
use Carp;
use Fatal qw/open close/;
use Tie::LogFile;



our $VERSION = substr(q$Revision: 1.1 $, 10);

my $globals = undef;

#---
# need to override import to send exported filehandles to calling script
#---
sub import{
        __PACKAGE__->export_to_level(2, @_);
}



#----
# since this a logger driver only PRC::Logger should be creating it
#----
sub new {
        Carp::croak "Only PRC::Logger should call me" unless caller eq 'PRC::Logger';

    my $proto = shift;
    my $args  = shift;
    my $class = ref($proto) || $proto;

    $globals = $args->{globals};


    my $self  = {
          logfile  => $args->{logfile}    || $globals->logname()
         ,errfile  => $args->{errfile}    || $globals->errlogname()
         ,logdir   => $args->{logdirname} || $globals->logdirname()
        };

    bless $self,$class;

    return $self;
}

sub showsettings {
        my $self = shift;
        #default initial logging
        print 'Scriptname: '. $globals->bindir() .'/'. $globals->scriptname() ."\n";
        print 'Effective User: '. $globals->effusername() ."\n";
        print 'Hostname: '. $globals->hostname() ."\n";
        print 'ErrFile: ' . $self->{logdir}.'/'.$self->{errfile} ."\n";
        print 'LogFile: ' . $self->{logdir}.'/'.$self->{logfile} ."\n";
}

sub start {
   my $self = shift;
   my $init = shift;

   tie(*PRCERR
      ,'Tie::LogFile'
      ,"$self->{logdir}/$self->{errfile}"
      ,format  => "%c (%p) [%d] %m"
      ,tformat => "%T %Y%m%d"
          ,autoflush => 1
      ) or Carp::croak "ERROR trying to tie PRCERR filehandle $!";

   tie(*PRCLOG, 'Tie::LogFile'
      ,"$self->{logdir}/$self->{logfile}"
      ,format  => '%c (%p) [%d] %m'
      ,tformat => '%T %Y%m%d'
          ,autoflush => 1
    ) or Carp::croak "ERROR trying to tie PRCLOG filehandle $!";


   #make the err and log files writable by group
   chmod(0664, $self->{logdir}.'/'.$self->{errfile} );
   chmod(0664, $self->{logdir}.'/'.$self->{logfile} );

   #by default do some basic initial logging.
   $self->initiallog() unless defined $init eq '0';
}

#----
# alias the file handles to stdout and stderr so print warn and die are redirected
#----
sub mkstdalias{
    shift;
    *STDOUT = *PRCLOG;
    *STDERR = *PRCERR;
}

#---
# can take an arrayref of records
#---
sub initiallog {
   my $self = shift;
   my $loglines = shift;

   if( $loglines ){
       foreach my $line(@$loglines) {
                   print PRCLOG $line;
       }
   }
   else {
       #default initial logging
       print PRCLOG 'Scriptname: '. $globals->bindir() .'/'. $globals->scriptname();
       print PRCLOG 'Effective User: '. $globals->effusername();
       print PRCLOG 'Hostname: '. $globals->hostname();
       print PRCLOG 'ErrFile: ' . $self->{logdir}.'/'.$self->{errfile};
       print PRCLOG 'LogFile: ' . $self->{logdir}.'/'.$self->{logfile};
   }
}

sub logargs{
    my $self = shift;
    my $opts = shift;

    foreach my $k(keys %$opts) {
                print PRCLOG "ARG: -$k $opts->{$k}";
    }
}

1;


__END__

=head1 NAME

  PRC::Logger::File

=head1 SYNOPSIS

  use PRC::Logger
  my $logger= PRC::Logger->new({ logdriver => 'File'  });
  $logger->start();

  print PRCLOG "this goes to logdir/file.log";
  print PRCERR "this goes to logdir/file.err";

  #alias PRCLOG and PRCERR filehandle to STDOUT and STDERR
  $logger->mkstdalias();

  print "this goes to file.log";
  warn "this goes to file.err";

=head1 DESCRIPTION

This uses Tie::LogFile modules and sets up default logfile and errfile
directories from PRC::Globals. This exports file handles PRCLOG and
PRCERR to the caller

=head1 METHODS

=over

=item new()

This creates the object with default logname, errname, and logdirname from PRC::Globals
Or the passed in PRC::Globals object from {globals =>} key. If no globals object is passed
it will create on with default configuration file or the file passed in with {config key}.

Possible overrides are passed in as a hashref

{
   logfile
   errfile
   logdirname

}

my $logger= PRC::Logger->new( {config => '/path/to/configuration.conf'} );

my $logger= PRC::Logger->new( { globals => PRC::Globals->new() } );

my $logger= PRC::Logger->new({logdirname=>/user/defined/place});


=item showsettings()

prints to stdout the current logfile,errfile,user,and script

=item start()

creates file handles PRCLOG and PRCERR writing to logfile and errfile
by default start() calls initiallog(). If you dont want this pass a string zero
like $obj->start("0")

=item mkstdalias()

alias stdout and stderr to write to PRCLOG and PRCERR. This enable print,warn, die statements to be logged with timestamp.

=item logargs(\%opts)

takes the hashref from C<Getopt::Std> passed to the script and logs them

=item initiallog()

writes either user input inthe form of an arrayref to the logfile or showsettings() info using the PRCLOG handle


=back

=head1 VERSION

$Id: File.pm,v 1.1 2008/06/04 23:03:17 axs Exp $

=cut
