#!/usr/bin/perl

#   DESCRIPTION:
#   DATE:         10/13/07 9:32:08 AM
#
#   $Id: ChallengeResponse.pm,v 1.1 2008/06/04 23:02:19 axs Exp $
#
#    $Author: axs $
#
#   Project:
#
#   Platform:
#
#   Purpose: simple challenge respnse auth functions
#



package PRC::Auth::ChallengeResponse;


require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = ( qw/getchallenge getresponse/ );

use strict;
use Digest::MD5 qw/md5/;



our $VERSION = substr(q$Revision: 1.1 $, 10,-1);


sub getchallenge{
        my @g = map{ chr(int(rand(255))) }(0..16);
        return join("",@g);
}

sub getresponse{
        my ($password, $challenge) = @_;

        my $ctx = Digest::MD5->new;
        $ctx->add($password);
        $ctx->add($challenge);
        return $ctx->digest;
}


1;


__END__


=head1 NAME

  PRC::Auth::ChallengeResponse

=head1 SYNOPSIS

  use strict;
  use PRC::Auth::ChallengeResponse qw/getresponse getchallenge/;

  my $c=getchallenge();
  #client
  my $r=getresponse("secret", $c);
  #server
  my $xx=getresponse("secret", $c);

  if ($xx eq $r) {
         print 'all good';
  }


=head1 DESCRIPTION

Simple Challenge response system

# server/client communication

# 1. client connects.  server issues challenge.

client connects to server

#server sends to client the challenge
$challenge = getchallenge()

# 2. client combines known password and challenge, and calculates
# the response
$client_response = getresponse("trustno1", $challenge)

sends the server $client_response

# 3. server does the same, and compares the result with the
# client response.  the result is a safe login in which the
# password is never sent across the communication channel.
$server_response = getresponse("trustno1", $challenge)

  if ($server_response eq $client_response){
          print "server:", "login ok"
  }


=head1 Functions

=over

=item getchallenge()

  returns a random seed

=item getresponse()

  concats random seed with known pass creating  a digest

=back

=head1 VERSION

$Id: ChallengeResponse.pm,v 1.1 2008/06/04 23:02:19 axs Exp $

=cut




1;
