#!/usr/bin/perl
#---------------------------------------------------
#   DESCRIPTION:
#   DATE:         8/14/07 6:41:36 PM
#   $Id: Server.pm,v 1.1 2007/08/15 00:03:29 axs Exp $
#---------------------------------------------------


package POE::Component::Heartbeat::Server;

use warnings;
use strict;

use FindBin qw($Bin);
use POE;
use IO::Socket::INET;
use MIME::Lite;
use POSIX;
use Time::HiRes qw/time/;

$|++;


sub create{
    shift;
    my $inargs = shift;
    my %server = ( PORT  => 38765
                   ,DATAGRAM_MAXLEN => 1024);
    my $host    = `hostname`;chomp($host);

    my %clients=();
    foreach my $c( @{$inargs->{clients}} ){
        $clients{$c}{started}  = undef;
        $clients{$c}{lastbeat} = undef;
    }

    POE::Session->create(
                         inline_states => {
                                           _start        => \&server_start
                                           ,get_datagram => \&server_read
                                           ,tick         => \&server_tick
                                          }
                         ,heap => {
                                  hostname => $host
                                 ,clients => \%clients
                                   ,server => \%server
                                 }
                        );
}

sub start {
    $poe_kernel->alias_set(__PACKAGE__);
}


sub server_tick{
    print "Checking clients \n";
    my %clients = %{$_[HEAP]->{clients}};

    my $time = time();

    foreach my $k( keys %clients ){
        next unless defined $clients{$k}{lastbeat};
        my $t = ConvertEpoch( $clients{$k}{lastbeat} );
        my $delta = $time - $clients{$k}{lastbeat} ;
        print "$k lastbeat $t delta: $delta\n";
    }

    $_[KERNEL]->delay( tick => 13 );
}



sub server_start {
    my $kernel = $_[KERNEL];
    my $heap = $_[HEAP];

    print qq^
      hostname: $heap->{hostname}
      port: $heap->{server}{PORT}
      ^;
    print "\nClients to check ...\n";
    map{print "$_\n"}keys %{$heap->{clients}};


    my $socket = IO::Socket::INET->new(
                                        Proto     => 'udp'
                                       ,LocalPort => $_[HEAP]->{server}{PORT}
                                      );

    die "Couldn't create server socket: $!" unless $socket;
    $kernel->delay( tick => 1 );
    $kernel->select_read( $socket, "get_datagram" );
}



sub server_read {
    my ( $kernel, $socket ) = @_[ KERNEL, ARG0 ];

    my $remote_address = recv( $socket, my $message = "",$_[HEAP]->{server}{DATAGRAM_MAXLEN}, 0 );
    return unless defined $remote_address;

    my ( $peer_port, $peer_addr ) = unpack_sockaddr_in($remote_address);
    my $human_addr = inet_ntoa($peer_addr);


    my $tepoch = time();
    my $t = ConvertEpoch( $tepoch );
    print "$t $human_addr : $peer_port sent us $message\n";


    #set the clients lastbeat time
    $_[HEAP]->{clients}{$message}{lastbeat} = $tepoch;
    $_[HEAP]->{clients}{$message}{started} = $tepoch
                 unless defined $_[HEAP]->{clients}{$message}{started};
}


sub ConvertEpoch{
    my $epoch=shift;
    return undef unless defined $epoch;
    my @e=localtime($epoch);

    return sprintf("%04d/%02d/%02d %02d:%02d:%02d"
                   ,($e[5]+1900),($e[4]+1),$e[3],$e[2],$e[1],$e[0]);
}


1;



__END__

use strict;
use POE;
use POE::Component::Heartbeat::Server;

POE::Component::Heartbeat::Server->create({clients => [ qw^fred wima^ ]});

POE::Kernel->run();
