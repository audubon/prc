#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:
#   DATE:         8/14/07 6:41:50 PM
#   $Id: Client.pm,v 1.1 2007/08/15 00:03:29 axs Exp $
#---------------------------------------------------



package POE::Component::Heartbeat::Client;



use warnings;
use strict;
use POE;
use IO::Socket::INET;

$|++;

use constant DATAGRAM_MAXLEN => 1024;

sub create{
    shift;
    my $inconf = shift;
    my $host   = `hostname`; chomp $host;
    my @e      = caller;
    my %conf = (
    BEATPORT => 38765
                ,BEATIP  => '135.68.58.185'
                ,BEATDUR => 7
               );

    $conf{CALLER} = $e[1] .':'. $host;

    if ( defined %$inconf ) {
        print 'kalled';
        foreach my $k( keys %$inconf ){
            $conf{$k} = $inconf->{$k};
        }
    }

    POE::Session->create(
                         inline_states => {
                                           _start       => \&client_start
                                           ,get_datagram => \&client_read
                                           ,beat => \&client_beat
                                          }

                         ,heap => \%conf
                        );
}

sub start {
    $poe_kernel->alias_set(__PACKAGE__);
}




sub client_beat{
    my $kernel = $_[KERNEL];
    print "heart beat \n";

    my $socket = IO::Socket::INET->new( Proto => 'udp' );
    die "Couldn't create client socket: $!" unless $socket;

    $kernel->select_read( $socket, "get_datagram" );

    my $message = $_[HEAP]->{CALLER};
    print "Sending $_[HEAP]->{MYNAME} '$message'\n";

    my $server_address = pack_sockaddr_in( $_[HEAP]->{BEATPORT}, inet_aton( $_[HEAP]->{BEATIP}) );
    send( $socket, $message, 0, $server_address ) == length($message)
                         or die "Trouble sending message: $!";

    $kernel->delay( beat => $_[HEAP]->{BEATDUR} );
}

sub client_start {
    my $kernel = $_[KERNEL];

    $_[KERNEL]->delay( beat => 10 );
}



1;


__END__

use strict;
use POE;
use POE::Component::Heartbeat::Client;

POE::Component::Heartbeat::Client->create( {BEATDUR =>3,MYNAME=>'Judy'} );
POE::Component::Heartbeat::Client->create({BEATDUR =>7,MYNAME=>'George'} );

POE::Kernel->run();

