#!/usr/bin/perl

#   DESCRIPTION:
#   DATE:         Thu Jan  3 10:45:46 CST 2008
#
#
#   $Id: XML.pm,v 1.2 2008/06/04 23:02:34 axs Exp $
#



package PRC::Config::XML;


use strict;
use Carp;
use XML::Simple;

our $AUTOLOAD;

sub new {
    my $proto = shift;
    my $args  = shift;
    my $class = ref($proto) || $proto;

        my $self         = {};
        $self->{xmlconf} = delete $args->{xmlconf} or Carp::croak "xmlconf is required $!";
        $self->{cfg}     = {};

    bless $self,$class;

    $self->__setConfig();

    return $self;
}


#---
#  sets the self->{cfg} to the XMLf files keys and values as returned from XMLin
#---
sub __setConfig{
        my $self = shift;
        my $cfg = undef;

        $self->{cfg} = XMLin($self->{xmlconf});
}


sub getConfKeys{
        my $self = shift;
        return keys %{$self->{cfg}};
}


#---
# accessors for configuration
# - getter : $obj->get_key() returns value
# - setter : $obj->set_key("value")
#---
sub AUTOLOAD{
        my $self = shift;
        my $type = ref($self) or croak "$self is not an object";

        my $name = $AUTOLOAD;
        $name =~ s/.*://;   # strip fully-qualified portion

        my($access, $key)= $name =~ m/^(set|get)_(.+)/;

        return undef if $name eq 'DESTROY';

        #unless (exists $self->{cfg}{$name} ) {
        #       croak "Cant access $name field in class $type";
        #}

        if( @_ && $access eq 'set' ){
                return $self->{cfg}{$key} = shift;
        }
        elsif( $access eq 'get' ){
                return $self->{cfg}{$key};
        }
}


1;

__END__

=head1 NAME

  PRC::Config::XML

=head1 SYNOPSIS

  use PRC::Config::XML;
  my $t = PRC::Config::XML->new({xmlconf => 'xmlfile.conf'});

  print $t->get_dsn();
  print $t->set_dsn('AIP');


=head1 DESCRIPTION

This reads an xml config file into a hash with autoloaded accessors

=head1 METHODS

=over

=item new()

This creates the object hashref args

  xmlconf =>  full path to xml config file


=item getConfKeys()

returns all available config keys

=item set_[keyname](value)

this will set the value to the key

=item get_[keyname]()

this returns the value of the [keyname]


=back

=head1 VERSION

$Id: XML.pm,v 1.2 2008/06/04 23:02:34 axs Exp $

=cut
