#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:
#   DATE:         10/13/07 9:32:08 AM
#   $Id: Entropy.pm,v 1.1 2007/10/13 15:46:15 axs Exp $
#---------------------------------------------------

package PRC::Entropy;

use strict;
use Carp;
use Fatal;

our $VERSION = substr(q$Revision: 1.1 $, 10);


sub new {
    my $proto = shift;
    my $args  = shift;
    my $class = ref($proto) || $proto;

    my $self  = {
           content  => $args->{content}   # []
          ,freqdist => $args->{freqdist} || undef
          ,probdist => $args->{probdist} || undef
        };

    bless $self,$class;

    return $self;
}


#build a frequecny distribution of elements in content
sub freqdistro {
    my $self = shift;
    my @letters= @{$self->{content}}; #grep{/[A-Za-z]/} split('',$self->{content});
    my %freqlet = ();
    map{ $freqlet{uc($_)}++ } @letters;

    $self->{freqdist} = \%freqlet;
}

#build prob distribution of content
sub probdistro {
    my $self      = shift;

    $self->freqdistro() unless defined $self->{freqdist};

    my $tottokens = scalar @{$self->{content}};
    my $uniqalpha = scalar keys %{$self->{freqdist}};
    my %wghtalpha = ();
    my $entropy   = 0;

    foreach my $t( keys %{$self->{freqdist}} ){
        $wghtalpha{$t} = $self->{freqdist}{$t} / $tottokens;
    }

    $self->{probdist} = \%wghtalpha;
}

sub entropy {
    my $self      = shift;

    $self->probdistro() unless defined $self->{probdist};

    my $uniqalpha = scalar keys %{$self->{freqdist}};
    my $entropy   = 0.0;

    foreach my $w ( keys %{$self->{probdist}} ){
        $entropy += $self->{probdist}{$w} * log2($self->{probdist}{$w});
    }

    my $maxentropy = log2($uniqalpha);
    my $bits       = -1.0 * $entropy;
    my $ratio      = $bits / $maxentropy;

    return wantarray ? ($entropy,$bits,$maxentropy,$ratio) : [$entropy,$bits,$maxentropy,$ratio];
}

sub log2 {
    return log($_[0]) / log(2);
}


1;
