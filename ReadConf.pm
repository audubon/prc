#!/usr/bin/perl

# $Id: ReadConf.pm,v 1.1 2008/06/04 23:01:38 axs Exp $
# $Author: axs $

package PRC::ReadConf;



require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = ( qw/retenv/ ); # symbols to export on request

use strict;
use File::Basename qw/dirname/;

$|++;

#---
# pass in a fullpath to the config or try to get the config from /etc
# if the dir structure is in /bin, /lib, /etc setup
#---
sub retenv {
   my %envarray;

   my $cfg = shift || dirname(__FILE__) . "/../../etc/aurora.conf";

   open CONFIG, "<$cfg" or die "couldn't find the conf file $cfg\n $!";
   while ( my $conf = <CONFIG> ) {
	   next unless $conf =~/\S/;
      my ( @confline ) = split " |\t", $conf;
      if ( $confline[0] =~ /^#/ || @confline == 0 ) {
		  next;
      }
      chomp $confline[1];
      $envarray{$confline[0]} = $confline[1];
   }

   close(CONFIG);
   return %envarray;
}

1;
