#!/usr/bin/perl

#   DESCRIPTION:
#   DATE:         Mon Nov  6 15:22:30 CST 2006
#
#   $Id: Globals.pm,v 1.2 2008/06/04 23:01:38 axs Exp $
#
#   Author:
#    $Author: axs $
#

package PRC::Globals;


use strict;
use Carp;
use Fatal       qw/open close/;
use FindBin     qw($Bin);
use Date::Manip qw/UnixDate ParseDate/;
use File::Basename qw/basename/;
use File::Path  qw/mkpath/;


our $VERSION = substr(q$Revision: 1.2 $, 10,-1);


sub new {
    my $proto = shift;
        my $args  = shift;
    my $class = ref($proto) || $proto;

    my $today=UnixDate(ParseDate('today'),'%Y%m%d');

        #try to set logbase from PRC::ReadConf module. If it errors have a default
        my %conf = ();
        my $logbasedir = '/var/data/logs/aurora';  #this is the default
        eval{
                local $SIG{__DIE__};
                require PRC::ReadConf;
                my $cfg = delete $args->{config};
                %conf = PRC::ReadConf::retenv($cfg);
        };
        unless( $@ ){
                $logbasedir = exists $conf{AURORAlog} ? $conf{AURORAlog} : $logbasedir;
        }


    my $self  = {
                  hostname  => undef
                 ,cwd       => undef
                 ,bindir    => undef
                 ,user      => 'nobody'
                 ,date      => $today
                 ,effuserid => $>
                 ,logbase   => $args->{logbase} || $logbasedir
                };

    bless $self,$class;

    return $self;
}

sub hostname {
   my $self = shift;
   $self->{hostname} ||=  `hostname`;
   chomp $self->{hostname};

   return $self->{hostname};
}

sub bindir {
   my $self = shift;
   $self->{bindir} ||= $Bin;

   return $self->{bindir};
}

sub effuserid {
   my $self = shift;
   return $self->{effuserid};
}

sub effusername {
   my $self = shift;
   return getpwuid($self->{effuserid});
}

sub date {
   my $self = shift;
   $self->{date} = shift || $self->{date};

   return UnixDate(ParseDate($self->{date}),'%Y%m%d');
}


#if arg passed will return scriptname without ext
sub scriptname {
        shift;
        my $a=shift;

        my $sbase=basename($0);
        if( $a ){
                return (split(/\./,$sbase))[0];
        }

        return $sbase;
}


sub logname {
        my $self=shift;
        return $self->scriptname(1) . '.log'
}


sub errlogname {
        my $self=shift;
        return $self->scriptname(1) . '.err'
}

sub logdirname {
        my $self = shift;

        my $logbase    = $self->{logbase};
        my $todaydate  = $self->date();
        my $scriptbase = $self->scriptname(1);
        my $logdir     = "$logbase/$scriptbase/$todaydate";

        mkpath $logdir unless -e $logdir;

        return $logdir;
}


sub logbase {
    my $self=shift;
    return $self->{logbase};
}



1;


=head1 NAME

  PRC::Globals

=head1 SYNOPSIS

  use PRC::Globals
  my $globals = PRC::Globals->new();

  my $globals = PRC::Globals->new({ config => '/path/to/configuration.conf' });

=head1 DESCRIPTION

This gives basic information on the running program

=head1 METHODS

=over

=item new()

constructor

=item logbase()

logbase directory


=item logname()

scriptname.log

=item errlogname()

scriptname.lerr


=item hostname()

name of the machine running script

=item bindir()

dir where script is running

=item effuserid()

effective user id

=item effusername()

effective user name


=back

=head1 VERSION

$Id: Globals.pm,v 1.2 2008/06/04 23:01:38 axs Exp $

=cut
