#!/usr/bin/perl


#
#   $Id: Bot.pm,v 1.1 2008/06/04 23:04:04 axs Exp $
#



package PRC::Qmessenger::Bot;

use strict;
use XML::Twig;
use LWP::Simple;
use Carp;


sub new {
    my $proto = shift;
    my $args  = shift;
    my $class = ref($proto) || $proto;

    my $self  = {
                                  botname => $args->{botname}    || 'aip_bot@aip.prc.com'
                                 ,botpass => $args->{botpass}  || 'mkqbot08'
                                 ,url     => $args->{url}        || 'http://bots.q.prc.com:8080/QBotService/BotTalk?xmlMessage='
                                 ,twig    => _twigobj()
                                };

    bless $self,$class;

    return $self;
}


#-----
# returns hashref of contacts => number of messages pending
#----
sub chkmsg{
        my $self = shift;
        my $msg=qq^<message type="contact.get.list" id="$self->{botname}" password="$self->{botpass}"  />^;

        my $content = get($self->{url} . $msg) or Carp::carp("Couldn't get $self->{url} $msg $!");
        return undef unless defined $content;

        $self->{twig}->parse($content);
        my $root= $self->{twig}->root;

        my @contacts = $root->children;

        my %pending = ();
        foreach my $c( @contacts ){
                next unless $c->{'att'}->{messageCount} gt 0;
                $pending{ $c->{'att'}->{id} } = $c->{'att'}->{messageCount};
        }
        $self->{twig}->purge();

        return \%pending;
}


#---
# getmsg('we1234')
# returns arrrayref of messages
#---
sub getmsg{
        my $self    = shift;
        my $contact = shift;
        my @messages= ();

        my $msg=qq^<message type="contact.get.messages" id="$self->{botname}" password="$self->{botpass}" contactId="$contact" />^;

        my $content = get($self->{url} . $msg) or Carp::carp("Couldn't get $self->{url} $msg  $!");

        print $content;
        return undef unless defined $content;

        $self->{twig}->parse($content);
        my $root= $self->{twig}->root;

        my @statements = $root->children;
        foreach my $s ( @statements ){
                push @messages, $s->{'att'}->{text};
        }

        \@messages;
}


#----
# sendmsg('po1234','hello')
#---
sub sendmsg{
        my $self   = shift;
        my $sendto = shift;
        my $txt    = shift;

        my $msg = qq^<message type="contact.send.message"  id="$self->{botname}" password="$self->{botpass}" contactId="$sendto" ><statement from="$self->{botname}" to="$sendto" text="$txt" /></message>^;

        my $content = get($self->{url} . $msg) or Carp::carp("Couldn't get $self->{url} $msg $!");
}




sub _twigobj{
        my $twig = XML::Twig->new();
        return $twig;
}


1;


__END__


=head1 Name

  PRC::QMessenger::Bot

=head1 SYNOPSIS


  use strict;
  use PRC::Qmessenger::Bot;

  my $bot = PRC::Qmessenger::Bot->new();

  my $t =$bot->chkmsg();
  foreach my $contact( keys %$t ){
          print "$contact sent $t->{$contact} messages \n";

          my $messages = $bot->getmsg($contact);
          foreach my $m( @$messages ){
                  print "Message:  $m\n";
          }
  }

  $bot->sendmsg('uy9664','Bring in pizza');

=head1 DESCRIPTION

This is a simple object to communicate with the Q message systems automated
bot interface.


=head1 METHODS

=over

=item new()

constructor. takes hashref of optional arguments
  botname => $args->{botname}
  ,botpass => $args->{botpass}


=item chkmsg()

returns hashref of attuid => number of messages

=item getmsg(attuid)

returns arrayref of text messages from passed in attuid

=item sendmsg(attuid,message)

sends a message to attuid

=back

=head1 VERSION

$Id: Bot.pm,v 1.1 2008/06/04 23:04:04 axs Exp $


=cut
